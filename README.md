# My awesome project

A spike to carry out deployment from GitLabCI to a Windows EC2 instance

## Steps

- [Server] Install [OpenSSH server](https://winscp.net/eng/docs/guide_windows_openssh_server) on Windows
- [Server] Set up [sshkey-based authentication](https://www.concurrency.com/blog/may-2019/key-based-authentication-for-openssh-on-windows). Pay extra attention to administrators authorization and permissions for the `administrators_authorized_keys` file.
- [GitLab] Add private key (for the public key authorized in the last step) to GitLab. Project > Settings > CI/CD > Variables
- [GitLab] Add server IP address also as a variable
- [Repository] Use the `.gitlab-ci.yml` script to push code to the EC2 Windows instance.

Note: For installation of SSH service, I tried [this](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse) first, but it didn't work.
